package com.example.inlaemning2_v2.game;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.UUID;

@Entity(name = "game")
@Table(name = "game")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GameEntity {

    @Id
    @Column(name = "gameId")
    private UUID gameId;
    @Column(name = "player1")
    private UUID player1;
    @Column(name = "player1Name")
    private String player1Name;
    @Column(name = "player1Move")
    private String player1Move;

    @Column(name = "player2")
    private UUID player2;
    @Column(name = "player2Name")
    private String player2Name;
    @Column(name = "player2Move")
    private String player2Move;

    @Column(name = "gameStatus")
    private String gameStatus;
}

