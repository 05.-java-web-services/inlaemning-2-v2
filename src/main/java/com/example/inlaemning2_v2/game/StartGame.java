package com.example.inlaemning2_v2.game;

import lombok.Value;

@Value
public class StartGame {
    String player1Name;
}
