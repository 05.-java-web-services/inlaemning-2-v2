package com.example.inlaemning2_v2.game;

import lombok.Value;
import java.util.UUID;

@Value
public class GameInfo {
    UUID gameId;
    String player1Name;
    String player1Move;
    String player2Name;
    String player2Move;
    String gameStatus;
}
