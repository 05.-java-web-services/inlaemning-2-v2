package com.example.inlaemning2_v2.game;

import lombok.Getter;

@Getter
public enum Status {
    NONE(0),
    OPEN(1),
    ACTIVE(2),
    WIN(3),
    LOSE(4),
    DRAW(5);

    private int status;

    Status(int status) {
        this.status = status;
    }
}

