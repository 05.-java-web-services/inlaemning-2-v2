package com.example.inlaemning2_v2;

import com.example.inlaemning2_v2.game.GameEntity;
import com.example.inlaemning2_v2.game.GameInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class GameController {

    GameService gameService;

    @Autowired
    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    // Hämtar en lista på spel som går att joina, filtrerad på statusen OPEN
    @GetMapping("/games")
    public List<GameInfo> gameList() {
        return gameService.gameList()
                .stream()
                .map(GameController::gameToDTO)
                .filter(s -> s.getGameStatus().equals("OPEN"))
                .collect(Collectors.toList());
    }

    // Spelare skapar ett nytt spel
    @PostMapping("/games/start")
    public GameInfo startGame(@RequestHeader(value = "Token") UUID id) {

        return gameToDTO(gameService.startGame(id));
    }

    // Spelare joinar ett spel
    @GetMapping("/games/join/{gameId}")
    public GameInfo joinGame(@RequestHeader(value = "Token") UUID id,
                             @PathVariable("gameId") UUID gameId) {

        return gameToDTO(gameService.joinGame(id, gameId));
    }

    // Spelare gör sitt drag
    @PostMapping("/games/move/{gameId}/{sign}")
    public GameInfo makeMove(@RequestHeader(value = "Token") UUID id,
                             @PathVariable("sign") String sign,
                             @PathVariable("gameId") UUID gameId) {

        return gameToDTO(gameService.makeMove(id, gameId, sign));
    }

    // Hämta status
    @GetMapping("/games/{gameId}")
    public GameInfo getStatus(@RequestHeader(value = "Token") UUID id,
                          @PathVariable("gameId") UUID gameId) {

        return gameToDTO(gameService.getStatus(id, gameId));
    }

    public static GameInfo gameToDTO(GameEntity gameEntity) {
        return new GameInfo(
                gameEntity.getGameId(),
                gameEntity.getPlayer1Name(),
                gameEntity.getPlayer1Move(),
                gameEntity.getPlayer2Name(),
                gameEntity.getPlayer2Move(),
                gameEntity.getGameStatus()
        );
    }
}
