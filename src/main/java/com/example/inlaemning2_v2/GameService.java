package com.example.inlaemning2_v2;

import com.example.inlaemning2_v2.game.GameEntity;
import com.example.inlaemning2_v2.game.GameRepository;
import com.example.inlaemning2_v2.game.Status;
import com.example.inlaemning2_v2.player.PlayerEntity;
import com.example.inlaemning2_v2.player.PlayerRepository;
import com.example.inlaemning2_v2.player.playermove.PlayerMove;
import com.example.inlaemning2_v2.player.playermove.PlayerMoveRepo;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class GameService {

    GameRepository gameRepository;
    PlayerRepository playerRepository;
    PlayerMoveRepo playerMoveRepo;

    public GameService(GameRepository gameRepository, PlayerRepository playerRepository, PlayerMoveRepo playerMoveRepo) {
        this.gameRepository = gameRepository;
        this.playerRepository = playerRepository;
        this.playerMoveRepo = playerMoveRepo;
    }

    // Hämtar en lista på spel som går att joina, filtrerad på statusen OPEN
    public List<GameEntity> gameList() {
        return gameRepository.findAll();
    }

    // Spelaren skapar ett nytt spel som får status "OPEN"
    public GameEntity startGame(UUID id) {
        Optional<PlayerEntity> optionalPlayerEntity = playerRepository.findById(id);
        GameEntity gameEntity = new GameEntity();

        if(optionalPlayerEntity.isPresent()) {
            gameEntity.setGameId(UUID.randomUUID());
            gameEntity.setPlayer1(optionalPlayerEntity.get().getPlayerId());
            gameEntity.setPlayer1Name(optionalPlayerEntity.get().getName());
            gameEntity.setGameStatus(String.valueOf(Status.OPEN));
        }
        gameRepository.save(gameEntity);

        return gameEntity;
    }

    // Spelare joinar ett spel, sätter denna spelare på player1. Spel får status "ACTIVE"
    public GameEntity joinGame(UUID id, UUID gameId) {
        Optional<PlayerEntity> optionalPlayerEntity = playerRepository.findById(id);
        GameEntity gameEntity = getGame(gameId);

        if(optionalPlayerEntity.isPresent()) {
            gameEntity.setPlayer2(gameEntity.getPlayer1());
            gameEntity.setPlayer2Name(gameEntity.getPlayer1Name());

            gameEntity.setPlayer1(optionalPlayerEntity.get().getPlayerId());
            gameEntity.setPlayer1Name(optionalPlayerEntity.get().getName());
            gameEntity.setGameStatus(String.valueOf(Status.ACTIVE));
        }
        gameRepository.save(gameEntity);

        return gameEntity;
    }

    /*
       Spelare gör sitt drag. Om spelaren som gör sitt drag inte syns på player 1,
       byts information så att spelaren ser sig själv som player 1
    */
    public GameEntity makeMove(UUID playerId, UUID gameId, String sign) {
        PlayerMove playerMove = new PlayerMove(playerId, sign);
        playerMoveRepo.save(playerMove);

        PlayerEntity playerEntity = getPlayer(playerId);
        GameEntity gameEntity = getGame(gameId);

        if(playerId.equals(gameEntity.getPlayer2())) {
            gameEntity.setPlayer2(gameEntity.getPlayer1());
            gameEntity.setPlayer2Name(gameEntity.getPlayer1Name());
            gameEntity.setPlayer2Move(gameEntity.getPlayer1Move());

            gameEntity.setPlayer1(playerMove.getPlayerId());
            gameEntity.setPlayer1Name(playerEntity.getName());
            gameEntity.setPlayer1Move(playerMove.getPlayerMove());
        } else {
            gameEntity.setPlayer1Move(sign);
        }

        return gameRepository.save(gameEntity);
    }

    /*
       Spelare efterfrågar status på spel. Om spelaren som efterfrågar status,
       inte ser sig själv på player 1, byts information så att spelaren ser sig själv som player 1.
       Här räknas även resultatet ut, om båda spelare har gjort sitt drag
    */
     public GameEntity getStatus(UUID playerId, UUID gameId) {
        GameEntity gameEntity = getGame(gameId);
        PlayerEntity playerEntity = getPlayer(playerId);
        PlayerMove playerMove = getMove(playerId);

        switchPlayer(playerId, gameEntity, playerEntity, playerMove);

        gameRepository.save(gameEntity);

        if(gameEntity.getPlayer2Move() == null) {
            gameEntity.setGameStatus(String.valueOf(Status.ACTIVE));
        } else if(gameEntity.getPlayer1Move().equals("rock")) {
            if (gameEntity.getPlayer2Move().equals("scissors")) {
                gameEntity.setGameStatus(String.valueOf(Status.WIN));
            } else if (gameEntity.getPlayer2Move().equals("paper")) {
                gameEntity.setGameStatus(String.valueOf(Status.LOSE));
            } else if (gameEntity.getPlayer2Move().equals("rock")) {
                gameEntity.setGameStatus(String.valueOf(Status.DRAW));
            }
        } else if(gameEntity.getPlayer1Move().equals("paper")) {
            if (gameEntity.getPlayer2Move().equals("rock")) {
                gameEntity.setGameStatus(String.valueOf(Status.WIN));
            } else if (gameEntity.getPlayer2Move().equals("scissors")) {
                gameEntity.setGameStatus(String.valueOf(Status.LOSE));
            } else if (gameEntity.getPlayer2Move().equals("paper")) {
                gameEntity.setGameStatus(String.valueOf(Status.DRAW));
            }
        } else if(gameEntity.getPlayer1Move().equals("scissors")) {
            if (gameEntity.getPlayer2Move().equals("paper")) {
                gameEntity.setGameStatus(String.valueOf(Status.WIN));
            } else if (gameEntity.getPlayer2Move().equals("rock")) {
                gameEntity.setGameStatus(String.valueOf(Status.LOSE));
            } else if (gameEntity.getPlayer2Move().equals("scissors")) {
                gameEntity.setGameStatus(String.valueOf(Status.DRAW));
            }
        }

        return gameRepository.save(gameEntity);
    }

    // Metod för att byta plats på spelare, baserat på vem som efterfrågar status
    private void switchPlayer(UUID playerId, GameEntity gameEntity, PlayerEntity playerEntity, PlayerMove playerMove) {
        if(playerId.equals(gameEntity.getPlayer2())) {
            gameEntity.setPlayer2(gameEntity.getPlayer1());
            gameEntity.setPlayer2Name(gameEntity.getPlayer1Name());
            gameEntity.setPlayer2Move(gameEntity.getPlayer1Move());

            gameEntity.setPlayer1(playerMove.getPlayerId());
            gameEntity.setPlayer1Name(playerEntity.getName());
            gameEntity.setPlayer1Move(playerMove.getPlayerMove());
        }
    }

    // Metod för att hämta rätt spelare baserat på playerId, för att undvika för mycket repetetiv kod
    public PlayerEntity getPlayer(UUID playerId) {
        Optional<PlayerEntity> optionalPlayerEntity = playerRepository.findById(playerId);
        PlayerEntity playerEntity = new PlayerEntity();
        if(optionalPlayerEntity.isPresent()) {
            playerEntity = optionalPlayerEntity.get();
        }
        return playerEntity;
    }

    // Metod för att hämta rätt drag baserat på playerId, för att undvika för mycket repetetiv kod
    public PlayerMove getMove(UUID playerId) {
        Optional<PlayerMove> optionalPlayerMove = playerMoveRepo.findById(playerId);
        PlayerMove playerMove = new PlayerMove();
        if(optionalPlayerMove.isPresent()) {
            playerMove = optionalPlayerMove.get();
        }
        return playerMove;
    }

    // Metod för att hämta rätt spel baserat på gameId, för att undvika för mycket repetetiv kod
    public GameEntity getGame(UUID id) {
        Optional<GameEntity> optionalGameEntity = gameRepository.findById(id);
        GameEntity gameEntity = new GameEntity();
        if(optionalGameEntity.isPresent()) {
            gameEntity = optionalGameEntity.get();
        }
        return gameEntity;
    }
}
