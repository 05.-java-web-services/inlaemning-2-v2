package com.example.inlaemning2_v2.player;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;
import java.util.UUID;

@Service
public class PlayerService {

    PlayerRepository playerRepository;

    @Autowired
    public PlayerService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    // Skapar en ny tom spelare, med endast ett UUID
    public PlayerEntity createPlayer() {
        PlayerEntity playerEntity = new PlayerEntity(
                UUID.randomUUID(),
                null
        );
        playerRepository.save(playerEntity);

        return playerEntity;
    }

    // Spelare ställer in namn
    public PlayerEntity setName(UpdatePlayer updatePlayer, UUID id) {
        Optional<PlayerEntity> optionalPlayerEntity = playerRepository.findById(id);
        if(optionalPlayerEntity.isPresent()) {
            optionalPlayerEntity.get().setName(updatePlayer.getName());
        }
        return playerRepository.save(optionalPlayerEntity.get());
    }
}
