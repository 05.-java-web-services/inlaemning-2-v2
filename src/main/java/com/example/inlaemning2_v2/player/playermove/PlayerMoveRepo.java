package com.example.inlaemning2_v2.player.playermove;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.UUID;

@Repository
public interface PlayerMoveRepo extends JpaRepository<PlayerMove, UUID> {
}
