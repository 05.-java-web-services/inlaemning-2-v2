package com.example.inlaemning2_v2.player;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.UUID;

@RestController
@RequestMapping("/api/user")
public class PlayerController {

    PlayerService playerService;

    @Autowired
    public PlayerController(PlayerService playerService) {
        this.playerService = playerService;
    }

    // Skapar en ny tom spelare, med endast ett UUID
    @GetMapping("/new")
    public UUID createPlayer() {
        return playerService.createPlayer()
                .getPlayerId();
    }

    // Spelare ställer in namn
    @PutMapping("/name")
    public void setName(@RequestHeader("Token") UUID id,
                        @RequestBody UpdatePlayer updatePlayer) {

        playerService.setName(updatePlayer, id);
    }
}
