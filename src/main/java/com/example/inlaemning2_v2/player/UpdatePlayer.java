package com.example.inlaemning2_v2.player;

import lombok.Value;

@Value
public class UpdatePlayer {
    String name;
}
