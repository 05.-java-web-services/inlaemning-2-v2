package com.example.inlaemning2_v2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Inlaemning2V2Application {

	public static void main(String[] args) {
		SpringApplication.run(Inlaemning2V2Application.class, args);
	}

}
